def bubblesort(array)
  n = array.length

  loop do
    swapped = false

    (n -1).times do |i|
      if array[i] > array[i + 1]
        array [i], array[i + 1] = array[i + 1], array[i]
        swapped= true
      end
    end
    break if not swapped
  end

  array
end


arr = [9, 8, 5, 1, 4, 2, 5, 9, 3, 6, 1, 7]

 
 p bubblesort(arr)
