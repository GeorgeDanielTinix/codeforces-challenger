http://codeforces.com/problemset/problem/478/B

B. Random Teams
time limit per test1 second
memory limit per test256 megabytes
inputstandard input
outputstandard output
n participants of the competition were split into m teams in some manner so that each team has at least one participant. After the competition each pair of participants from the same team became friends.

Your task is to write a program that will find the minimum and the maximum number of pairs of friends that could have formed by the end of the competition.

Input
The only line of input contains two integers n and m, separated by a single space (1 ≤ m ≤ n ≤ 109) — the number of participants and the number of teams respectively.

Output
The only line of the output should contain two integers kmin and kmax — the minimum possible number of pairs of friends and the maximum possible number of pairs of friends respectively.

puts "Enter first number!"
m = gets.chomp.to_i

puts "Enter second number! "
n = gets.chomp.to_i
count = Array.new

kmax = n - m + 1

kmax = kmax*(kmax - 1)/2


kmin = (n/m)*((n/m)-1)/2


if (n%m == 0) then
  kmin *= m
else
  kmin *= m - -(n%m)
end

lftovrs = ((n/m) + 1)/2
kmin += lftovrs * (n/m)

middle_number = 0
count = kmin << middle_number << kmax

puts middle_number
puts kmax
puts kmin
